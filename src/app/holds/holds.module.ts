import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../shared/material.module';
import { HoldsTableComponent } from './holds-table/holds-table.component';
import { HoldsComponent } from './holds.component';
import { SimulationService } from './holds-table/simulation.service';
import { FormsModule } from '@angular/forms';
import { PropsModule } from '../props/props.module';

@NgModule({
    imports: [CommonModule, MaterialModule, FormsModule, PropsModule],
    declarations: [HoldsComponent, HoldsTableComponent],
    exports: [HoldsComponent],
    providers: [SimulationService]
})
export class HoldsModule {}