import { Component, OnInit } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { map, take } from 'rxjs/operators';
import { Observable, timer, zip } from "rxjs";
import { IHolds } from './interfaces/holds.interface';
import { RecordsWriterService } from "../shared/records/records-writer.service";
import { PropsService } from "src/app/props/services/props.service";
import { WagerTypesEnum } from "../shared/common/enums/wager-types.enum";
import { IPropsResult } from "../props/interfaces/props-result.interface";
import { Team } from 'src/app/shared/common/interfaces/teams.interface';
import { IPropsRecordUpdateRequest } from "../props/interfaces/props-record-update-request.interface";

interface IHoldsResponse {
    fanduel: IHolds[];
    draftkings: IHolds[];
    pointsbet: IHolds[];
}

@Component({
    selector: 'holds-component',
    templateUrl: './holds.component.html',
    styleUrls: ['./holds.component.scss']
})
export class HoldsComponent implements OnInit {
    public allAffiliateHolds: IHoldsResponse = null;
    public teams: Team[]
    public matchup: string[] = [];
    public searchKey: string;
    public saveStatus: boolean = null;
    public propsMap: {[key: string]: IPropsResult[]} = {};

    constructor(
        private http: HttpClient,
        private propsService: PropsService,
        private recordsWriterService: RecordsWriterService
    ) {}

    ngOnInit() {
        zip(this._getTeams(), this.getHolds())
            .pipe(take(1)).subscribe( ([teams, holds]) => {
                this.allAffiliateHolds = holds;
                this.teams = teams;
            }, err => {
                console.error(err);
            });
    }

    public getHolds(): Observable<IHoldsResponse> {
        return this.http.get('//localhost:8080/holds').pipe(
            map( (data: IHoldsResponse) => {
                Object.keys(data).forEach(key => {
                    data[key] = data[key].map((element: any) => {
                        element['markets']['eventId'] = element['eventId']
                        return element['markets'];
                    })
                });
                return data;
            })
        );
    }

    public onTeamSelect($event) {
        this.searchKey = this._setMatchupKey(this.matchup);
    }

    public saveWagers() {
        const records = [];
        for(let affiliate in this.allAffiliateHolds) {
            this.allAffiliateHolds[affiliate].forEach(hold => {
                for(let market in hold) {
                    if(!!hold[market]?.sides) {
                        hold[market].sides?.forEach( (side, index) => {
                            if(side.pick) {
                                const record = this.recordsWriterService.createRecord(market as WagerTypesEnum, hold, index);
                                records.push(record);
                            }  
                        });
                    }
                }
            });
        }
        this.recordsWriterService.submitRecords(records)
            .pipe(take(1)).subscribe(status => {
                this.showSaveStatus(status);
            });
    }

    public showSaveStatus(status: boolean) {
        this.saveStatus = status;
        timer(5000).pipe(take(1)).subscribe(() => this.saveStatus = null);
    }

    public getProps(propRequest: {hold: IHolds, affiliate: string}) {
        this.propsService.getProps(propRequest.hold.eventId, propRequest.affiliate,
            propRequest.hold.moneyline.sides[0].teamName, propRequest.hold.moneyline.sides[1].teamName)
            .pipe(take(1))
            .subscribe(result => {
                result.forEach(prop => prop.affiliate = propRequest.affiliate);
                const key =  this._setMatchupKey([propRequest.hold.moneyline.sides[0].teamName, propRequest.hold.moneyline.sides[1].teamName]);
                if(this.propsMap[key]) {
                    this.propsMap[key].push(...result);
                    this.propsMap[key].sort( (a, b) => {
                        if(a.playerName < b.playerName) { return -1; }
                        if(a.playerName > b.playerName) { return 1; }
                        return 0;
                    });
                } else {
                    this.propsMap[key] = result;
                    this.propsMap[key].sort( (a, b) => {
                        if(a.playerName < b.playerName) { return -1; }
                        if(a.playerName > b.playerName) { return 1; }
                        return 0;
                    });
                }
            });
    }

    public saveProps(savedPicks: IPropsRecordUpdateRequest[]) {
        this.propsService.savePicks(savedPicks).pipe(take(1)).subscribe(saveSuccessful => {
            this.showSaveStatus(saveSuccessful);
        });
    }

    private _getTeams(): Observable<Team[]> {
        return this.http.get('//localhost:8080/sports/NBA/teams')
            .pipe(
                take(1),
                map((teams: Team[]) => {
                    return teams.sort((a, b) => {
                        var textA = a.teamName.toUpperCase();
                        var textB = b.teamName.toUpperCase();
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    });
                })
            );
    }

    private _setMatchupKey(teams: string[]): string {
        return teams.sort( (a, b) => {
            if(a < b) { return -1; }
            if(a > b) { return 1; }
            return 0;
        }).join('/');
    }
}