import { IPropsResult } from "src/app/props/interfaces/props-result.interface";
import { IMonteCarloResults } from "src/app/stats/interfaces/monte-carlo-results.interface";

export interface ISide {
    odds: Number;
    teamName?: string;
    points?: Number;
    overUnder?: string;
    //FE field
    pick?: boolean;
}

export interface IHolds {
    eventId: string;
    spread: {
        sides: ISide[]
        hold: number;
    };
    total: {
        sides: ISide[],
        hold: number;
    },
    moneyline: {
        sides: ISide[],
        hold: number;
    }
    simulation?: IMonteCarloResults;
}