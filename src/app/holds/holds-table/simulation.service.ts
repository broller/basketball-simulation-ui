import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { IHolds, ISide } from "../interfaces/holds.interface";
import { Observable, of } from "rxjs";
import { IMonteCarloResults } from './../../stats/interfaces/monte-carlo-results.interface';

@Injectable()
export class SimulationService {
    constructor(private http: HttpClient) {}

    public runSimulation(hold: IHolds): Observable<IMonteCarloResults> {
        const teams = this._getFavorite(hold.moneyline.sides);
        const total = this._getTotal(hold.total.sides);
        const spread = this._getSpread(hold.spread.sides);
        const ml = this._getMoneyline(hold.moneyline.sides);
        
        const formObj = { ...teams, ...total, ...spread, ...ml }
        return this.http.post<IMonteCarloResults>('//localhost:8080/stats/NBA/affiliate/game', formObj);
    }

    private _getFavorite(sides: ISide[]): {favorite: String, underdog: String} {
        if(sides[0].odds < sides[1].odds) {
            return {favorite: sides[0].teamName, underdog: sides[1].teamName};
        } else {
            return {favorite: sides[1].teamName, underdog: sides[0].teamName};
        }
    }

    private _getTotal(sides: ISide[]): { total: Number, overOdds: Number, underOdds: Number } {
        if(sides[0].overUnder.toLowerCase() === 'o') {
            return { total: sides[0].points, overOdds: sides[0].odds, underOdds: sides[1].odds }
        } else {
            return { total: sides[0].points, overOdds: sides[1].odds, underOdds: sides[0].odds }
        }
    }

    private _getSpread(sides: ISide[]): {spread: Number, favoriteSpreadOdds: Number, underdogSpreadOdds: Number} {
        if(sides[0].points < 0) {
            return {spread: sides[0].points, favoriteSpreadOdds: sides[0].odds, underdogSpreadOdds: sides[1].odds};
        } else {
            return {spread: sides[1].points, favoriteSpreadOdds: sides[1].odds, underdogSpreadOdds: sides[0].odds};
        }
    }

    private _getMoneyline(sides: ISide[]): {favoriteMLOdds: Number, underdogMLOdds: Number} {
        if(sides[0].odds < sides[1].odds) {
            return {favoriteMLOdds: sides[0].odds, underdogMLOdds: sides[1].odds};
        } else {
            return {favoriteMLOdds: sides[1].odds, underdogMLOdds: sides[0].odds};
        }
    }
}