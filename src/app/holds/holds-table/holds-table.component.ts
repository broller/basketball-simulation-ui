import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { take } from "rxjs/operators";
import { IHolds } from '../interfaces/holds.interface';
import { SimulationService } from "./simulation.service";
import { PropsService } from "src/app/props/services/props.service";
import { IPropsResult } from "src/app/props/interfaces/props-result.interface";
import { TotalsEnum } from "src/app/shared/common/enums/wager-types.enum";
import { IPropsRecordUpdateRequest } from "src/app/props/interfaces/props-record-update-request.interface";

@Component({
    selector: 'holds-table',
    templateUrl: './holds-table.component.html',
    styleUrls: ['./holds-table.component.scss'],
    animations: [
      trigger('detailExpand', [
        state('collapsed', style({height: '0px', minHeight: '0'})),
        state('expanded', style({height: '*'})),
        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      ])],
})
export class HoldsTableComponent implements OnInit, AfterViewInit {
    @Input() public data: IHolds[];
    @Input() public affiliate: string;

    @Output() propSaveStatus: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() onRetrieveProps: EventEmitter<{hold: IHolds, affiliate: string}> = new EventEmitter<{hold: IHolds, affiliate: string}>();

    @Input() set filter(filterValue: string) {
        if(!!this.dataSource) {
            this.dataSource.filterPredicate = (data: IHolds, filter: string) => {
              const [team1, team2] = filter.split('/');
              if(!!team1 && !!team2) {
                const selectedTeam1 = data.moneyline?.sides[0].teamName.toLowerCase();
                const selectedTeam2 = data.moneyline?.sides[1].teamName.toLowerCase();
                return (selectedTeam1 === team1 || selectedTeam1 === team2) && (selectedTeam2 === team1 || selectedTeam2 === team2);
              } else {
                return true;
              }
            };
            this.dataSource.filter = filterValue?.trim().toLowerCase();
        }
    }

    @ViewChild(MatSort) sort: MatSort;

    dataSource: MatTableDataSource<IHolds> = null;
    displayedColumns: string[] = ['name', 'spread', 'spreadHold', 'save spread', 'total', 'totalHold', 'save total', 'moneyline', 'moneylineHold', 'save moneyline','simulate', 'props'];

    public overUnderEnum = TotalsEnum;
    constructor(private simService: SimulationService, private propsService: PropsService){}

    ngOnInit() {
        this.dataSource = new MatTableDataSource(this.data);
        //set a new filterPredicate function
        this.dataSource.filterPredicate = (data, filter: string) => {
            const accumulator = (currentTerm, key) => {
                return this.nestedFilterCheck(currentTerm, data, key);
            };
            const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
            // Transform the filter by converting it to lowercase and removing whitespace.
            const transformedFilter = filter.trim().toLowerCase();
            return dataStr.indexOf(transformedFilter) !== -1;
        }
    }

    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
        this.setDataAccesor();
    }

    public runSim(hold: IHolds) {
      this.simService.runSimulation(hold)
        .pipe(take(1)).subscribe(result => {
          hold.simulation = result;
        });
    }

    public getProps(hold: IHolds) {
      this.onRetrieveProps.emit({hold, affiliate: this.affiliate});
    }

    public onPropSelect($event, prop: IPropsResult, overUnderEnum: TotalsEnum) {
      if ($event.checked) {
        prop.pick = overUnderEnum;
      } else {
        prop.pick = null;
      }
    }

    public saveProps(props: IPropsResult []) {
      const savedPicks = []
      for(let prop of props) {
        if(prop.pick === this.overUnderEnum.OVER || prop.pick === this.overUnderEnum.UNDER) { 
          const pick: IPropsRecordUpdateRequest = {
            playerName: prop.playerName,
            value: prop.value,
            side: prop.pick,
            odds: prop.pick === this.overUnderEnum.OVER ? prop.overOdds : prop.underOdds,
            edge: prop.pick === this.overUnderEnum.OVER ? prop.overEdge : prop.underEdge,
            propType: 'PTS'
          }      
          savedPicks.push(pick)
        }
      }
      this.propsService.savePicks(savedPicks).pipe(
        take(1)
      ).subscribe(saveSuccessful => {
        this.propSaveStatus.emit(saveSuccessful);
      });
    }

    private setDataAccesor() {
        this.dataSource.sortingDataAccessor = (item: IHolds, property) => {
            if (property === 'totalHold') {
              return item.total.hold;
            } else if(property === 'spreadHold') {
                return item.spread.hold;
            } else if(property === 'moneylineHold') {
                return item.moneyline.hold;
            } else {
              return item[property];
            }
          };
    }

    private nestedFilterCheck(search, data, key) {
        if (typeof data[key] === 'object') {
          for (const k in data[key]) {
            if (data[key][k] !== null) {
              search = this.nestedFilterCheck(search, data[key], k);
            }
          }
        } else {
          search += data[key];
        }
        return search;
      }
}