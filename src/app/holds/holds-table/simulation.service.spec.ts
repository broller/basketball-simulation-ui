import { HttpClient } from "@angular/common/http";
import { TestBed, inject } from "@angular/core/testing"
import { Observable, of } from "rxjs";
import { take } from "rxjs/operators";
import { SimulationService } from "./simulation.service"

class MockHttpClient {
    post<IMonteCarloResults>(url: String, formObject: any): Observable<any> {
        return of(formObject);
    }
}

describe('Simulation Service: ', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {provide: HttpClient, useClass: MockHttpClient},
                SimulationService
            ],
        })
    });

    it('should exist', inject([SimulationService], (service: SimulationService) => {
        expect(service).toBeTruthy();
    }));


    it('should determine favorites correctly', inject([SimulationService], (service: SimulationService) => {
        let sides = [{teamName: 'DEN', odds: -130}, {teamName: 'ATL', odds: 110}]
        // @ts-ignore
        let result = service._getFavorite(sides);
        expect(result.favorite).toEqual('DEN');
        
        sides = [{teamName: 'DEN', odds: -130}, {teamName: 'ATL', odds: -110}];
         // @ts-ignore
        result = service._getFavorite(sides);
        expect(result.favorite).toEqual('DEN');

        sides = [{teamName: 'DEN', odds: 130}, {teamName: 'ATL', odds: -110}];
         // @ts-ignore
        result = service._getFavorite(sides);
        expect(result.favorite).toEqual('ATL');
    }));

    it('should determine spread info correctly', inject([SimulationService], (service: SimulationService) => {
        let sides = [{teamName: 'DEN', odds: -130, points: -3.5}, {teamName: 'ATL', odds: 110, points: 3.5}]
        // @ts-ignore
        let result = service._getSpread(sides);
        expect(result.favoriteSpreadOdds).toEqual(-130);
        expect(result.spread).toEqual(-3.5);
        expect(result.underdogSpreadOdds).toEqual(110);

        sides = [{teamName: 'DEN', odds: 130, points: 3.5}, {teamName: 'ATL', odds: -110, points: -3.5}];
         // @ts-ignore
        result = service._getSpread(sides);
        expect(result.favoriteSpreadOdds).toEqual(-110);
        expect(result.spread).toEqual(-3.5);
        expect(result.underdogSpreadOdds).toEqual(130);
    }));

    it('should determine moneyline info correctly', inject([SimulationService], (service: SimulationService) => {
        let sides = [{teamName: 'DEN', odds: -130, points: -3.5}, {teamName: 'ATL', odds: 110, points: 3.5}]
        // @ts-ignore
        let result = service._getMoneyline(sides);
        expect(result.favoriteMLOdds).toEqual(-130);
        expect(result.underdogMLOdds).toEqual(110);

        sides = [{teamName: 'DEN', odds: 130, points: 3.5}, {teamName: 'ATL', odds: -110, points: -3.5}];
         // @ts-ignore
        result = service._getMoneyline(sides);
        expect(result.favoriteMLOdds).toEqual(-110);
        expect(result.underdogMLOdds).toEqual(130);
    }));

    it('should determine total info correctly', inject([SimulationService], (service: SimulationService) => {
        let sides = [{teamName: 'DEN', points: 233.5, overUnder: 'o', odds: -112}, {teamName: 'ATL', points: 233.5, overUnder: 'u', odds: -109}]
        // @ts-ignore
        let result = service._getTotal(sides);
        expect(result.overOdds).toEqual(-112);
        expect(result.underOdds).toEqual(-109);

        sides = [{teamName: 'DEN', points: 233.5, overUnder: 'u', odds: -112}, {teamName: 'ATL', points: 233.5, overUnder: 'o', odds: -109}]
        // @ts-ignore
        result = service._getTotal(sides);
        expect(result.overOdds).toEqual(-109);
        expect(result.underOdds).toEqual(-112);

        sides = [{teamName: 'DEN', points: 233.5, overUnder: 'O', odds: -112}, {teamName: 'ATL', points: 233.5, overUnder: 'u', odds: -109}]
        // @ts-ignore
        result = service._getTotal(sides);
        expect(result.overOdds).toEqual(-112);
        expect(result.underOdds).toEqual(-109);
    }));
});