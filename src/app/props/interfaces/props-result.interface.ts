import { TotalsEnum } from "src/app/shared/common/enums/wager-types.enum";

export interface IPropsResult {
    playerName: String;
    value: number
    overOdds: number;
    underOdds: number;
    overEdge: number;
    underEdge: number;
    pick?: TotalsEnum;
    affiliate?: string;
}