import { TotalsEnum } from "src/app/shared/common/enums/wager-types.enum";

export interface IPropsRecordUpdateRequest {
    playerName: String;
    value: number;
	odds: Number;
	side: TotalsEnum;
	propType: String;
    edge: Number;
}