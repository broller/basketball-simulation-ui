import { Component, EventEmitter, Input, Output } from "@angular/core";
import { IPropsResult } from "../interfaces/props-result.interface";
import { TotalsEnum } from "src/app/shared/common/enums/wager-types.enum";
import { PropsService } from "src/app/props/services/props.service";
import { IPropsRecordUpdateRequest } from "../interfaces/props-record-update-request.interface";

@Component({
    selector: 'props-container',
    templateUrl: './props.component.html',
    styleUrls: ['./props.component.scss']
})
export class PropsContainer {
    @Input() public props: IPropsResult[];
    
    @Output() public onPropSave: EventEmitter<IPropsRecordUpdateRequest[]> = new EventEmitter<IPropsRecordUpdateRequest[]>();

    public overUnderEnum = TotalsEnum;
    public teams: string[];

    constructor(private propsService: PropsService){}

    public onPropSelect($event, prop: IPropsResult, overUnderEnum: TotalsEnum) {
        if ($event.checked) {
          prop.pick = overUnderEnum;
        } else {
          prop.pick = null;
        }
      }

    public saveProps(props: IPropsResult []) {
      const savedPicks = []
      for(let prop of props) {
        if(prop.pick === this.overUnderEnum.OVER || prop.pick === this.overUnderEnum.UNDER) { 
          const pick: IPropsRecordUpdateRequest = {
            playerName: prop.playerName,
            value: prop.value,
            side: prop.pick,
            odds: prop.pick === this.overUnderEnum.OVER ? prop.overOdds : prop.underOdds,
            edge: prop.pick === this.overUnderEnum.OVER ? prop.overEdge : prop.underEdge,
            propType: 'PTS'
          }      
          savedPicks.push(pick)
        }
      }
      this.onPropSave.emit(savedPicks);
    }
}