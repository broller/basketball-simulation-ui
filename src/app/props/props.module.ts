import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { PropsContainer } from "./components/props.component";
import { PropsService } from "./services/props.service";
import { MaterialModule } from "../shared/material.module";

@NgModule({
    imports: [CommonModule, MaterialModule],
    declarations: [PropsContainer],
    exports: [PropsContainer],
    providers: [PropsService]
})
export class PropsModule {}