import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPropsResult } from '../interfaces/props-result.interface';
import { Observable } from 'rxjs';
import { IPropsRecordUpdateRequest } from '../interfaces/props-record-update-request.interface';

@Injectable()
export class PropsService {
    constructor(private http: HttpClient) {}

    public getProps(eventId: String, affiliate: String, team1: String, team2: String): Observable<IPropsResult[]> {
        const formObj = {
            affiliate,
            eventId,
            teams: [team1, team2]
        }
        return this.http.post<IPropsResult[]>('//localhost:8080/stats/NBA/game/props/', formObj);
    }

    public savePicks(picks: IPropsRecordUpdateRequest[]): Observable<boolean> {
        const options = {headers: {'Content-Type': 'application/json'}};
        return this.http.post<boolean>('//localhost:8080/records/update/props/', picks, options);
    }
}