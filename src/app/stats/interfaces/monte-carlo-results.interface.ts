export interface IMonteCarloResults {
    favoriteMLEdge: number;
    favoriteMLPct: number;
    favoriteSpreadEdge: number;
    favoriteSpreadPct: number;
    overEdge: number;
    overPct: number;
    underEdge: number;
    underPct: number;
    underdogMLEdge: number;
    underdogMLPct: number;
    underdogSpreadEdge: number;
    underdogSpreadPct: number;
}