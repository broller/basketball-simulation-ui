import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material.module';
import { StatsContainerComponent } from './components/stats-container/stats.container.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, FormsModule, MaterialModule, ReactiveFormsModule],
    declarations: [StatsContainerComponent],
    exports: [StatsContainerComponent]
})
export class StatsModule {}