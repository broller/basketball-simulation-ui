import { Component, OnInit } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from "rxjs";
import { take, map } from "rxjs/operators";
import { Team } from 'src/app/shared/common/interfaces/teams.interface';
import { IMonteCarloResults } from '../../interfaces/monte-carlo-results.interface';

@Component({
    selector: 'stats-container',
    templateUrl: './stats-container.template.html',
    styleUrls: ['./stats-container.component.scss']
})
export class StatsContainerComponent implements OnInit {
    public teams: Team[];
    public form: FormGroup;
    
    public monteCarloResults: IMonteCarloResults;

    constructor(
        private formBuilder: FormBuilder,
        private http: HttpClient) {}

    ngOnInit() {
        this._getTeams().subscribe(teams => {
            this.teams = teams;
        });
        this.form = this.formBuilder.group({
            favorite: [null, [Validators.required]],
            underdog: [null, Validators.required],
            total: [null, [Validators.required]],
            overOdds: [null, [Validators.required]],
            underOdds:  [null, [Validators.required]],
            spread: [null, [Validators.required]],
            favoriteSpreadOdds: [null, [Validators.required]],
            underdogSpreadOdds: [null, [Validators.required]],
            favoriteMLOdds: [null, [Validators.required]],
            underdogMLOdds: [null, [Validators.required]]
          });
    }

    private _getTeams(): Observable<Team[]> {
        return this.http.get('//localhost:8080/sports/NBA/teams')
            .pipe(
                take(1),
                map((teams: Team[]) => {
                    return teams.sort((a, b) => {
                        var textA = a.teamName.toUpperCase();
                        var textB = b.teamName.toUpperCase();
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    });
                })
            );
    }

    public submit() {
        if(this.form.valid) {
            this.form.disable();
            this.http.post('//localhost:8080/stats/NBA/game', this.form.value)
            .pipe(take(1)).subscribe((results: IMonteCarloResults) => {
                console.log(results);
                this.monteCarloResults = results;
                this.form.enable();
            }, err => {
                console.error(err);
                this.form.enable();
            });
        }
    }

    public resetForm() {
        this.form.reset({
            favorite: [null, [Validators.required]],
            underdog: [null, Validators.required],
            total: [null, [Validators.required]],
            overOdds: [null, [Validators.required]],
            underOdds:  [null, [Validators.required]],
            spread: [null, [Validators.required]],
            favoriteSpreadOdds: [null, [Validators.required]],
            underdogSpreadOdds: [null, [Validators.required]],
            favoriteMLOdds: [null, [Validators.required]],
            underdogMLOdds: [null, [Validators.required]]
          });
        this.monteCarloResults = null;
    }
}