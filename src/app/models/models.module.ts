import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ModelsContainerComponent } from "./models-container.component";
import { PaceModelService } from "./pace/services/pace-model.service";
import { MaterialModule } from "../shared/material.module";
import { ModelsTableComponent } from "./models-table/models-table.component";
import { FormsModule } from "@angular/forms";

@NgModule({
    imports: [CommonModule, MaterialModule, FormsModule],
    declarations: [ModelsContainerComponent, ModelsTableComponent],
    exports: [ModelsContainerComponent],
    providers: [PaceModelService]
})
export class ModelsModule{}