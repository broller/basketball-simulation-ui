import { animate, state, style, transition, trigger } from "@angular/animations";
import { AfterViewInit, Component, Input, OnInit, ViewChild } from "@angular/core";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { IEvent } from "../interfaces/event.interface";

@Component({
    selector: 'models-table',
    templateUrl: './models-table.component.html',
    styleUrls: ['./models-table.component.scss'],
    animations: [
        trigger('detailExpand', [
          state('collapsed', style({height: '0px', minHeight: '0'})),
          state('expanded', style({height: '*'})),
          transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ])],
})
export class ModelsTableComponent implements OnInit, AfterViewInit {
    @Input() public data: IEvent[];
    @Input() public affiliate: string;
    @Input() set filter(filterValue: string) {
      if(!!this.dataSource) {
        // this.dataSource.filter = filterValue;
          this.dataSource.filterPredicate = (data: IEvent, filter: string) => {
            const [team1, team2] = filter.split('/');
            if(!!team1 && !!team2) {
              const selectedTeam1 = data.moneyline?.sides[0].teamName.toLowerCase();
              const selectedTeam2 = data.moneyline?.sides[1].teamName.toLowerCase();
              return (selectedTeam1 === team1 || selectedTeam1 === team2) && (selectedTeam2 === team1 || selectedTeam2 === team2);
            } else {
              return true;
            }
          };
          this.dataSource.filter = filterValue?.trim().toLowerCase();
      }
  }

    @ViewChild(MatSort) sort: MatSort;

    dataSource: MatTableDataSource<IEvent> = null;
    displayedColumns: string[] = ['name', 'spread', 'spreadHold', 'save spread',  'total', 'totalHold', 'save total', 'moneyline', 'moneylineHold', 'save moneyline'];

    ngOnInit() {
        this.dataSource = new MatTableDataSource(this.data);
        //set a new filterPredicate function
        this.dataSource.filterPredicate = (data, filter: string) => {
            const accumulator = (currentTerm, key) => {
                return this.nestedFilterCheck(currentTerm, data, key);
            };
            const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
            // Transform the filter by converting it to lowercase and removing whitespace.
            const transformedFilter = filter.trim().toLowerCase();
            return dataStr.indexOf(transformedFilter) !== -1;
        }
    }

    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
        this.setDataAccesor();
    }

    private nestedFilterCheck(search, data, key) {
        if (typeof data[key] === 'object') {
          for (const k in data[key]) {
            if (data[key][k] !== null) {
              search = this.nestedFilterCheck(search, data[key], k);
            }
          }
        } else {
          search += data[key];
        }
        return search;
    }

    private setDataAccesor() {
        this.dataSource.sortingDataAccessor = (item: IEvent, property) => {
            if (property === 'totalHold') {
              return item.total.hold;
            } else if(property === 'spreadHold') {
                return item.spread.hold;
            } else if(property === 'moneylineHold') {
                return item.moneyline.hold;
            } else {
              return item[property];
            }
          };
    }
}