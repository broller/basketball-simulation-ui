import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { IEventResponse } from "../../interfaces/event-response.interface";

@Injectable()
export class PaceModelService {
    constructor(private http: HttpClient){}

    public getPaceModelResults(): Observable<IEventResponse> {
        return this.http.get('//localhost:8080/stats/NBA/affiliate/model/pace').pipe(
            map( (data) => {
                Object.keys(data).forEach(key => {
                    data[key] = data[key].map((element: any) => {
                        element['markets']['eventId'] = element['eventId']
                        element['markets']['simulationResults'] = element['simulationResults']
                        return element['markets'];
                    })
                });
                return data as IEventResponse;
            })
        );
    }
}