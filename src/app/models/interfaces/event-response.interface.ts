import { IEvent } from "./event.interface";

export interface IEventResponse {
    [affiliate: string]: IEvent[];
}