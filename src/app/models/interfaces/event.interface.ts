import { ISide } from "src/app/holds/interfaces/holds.interface";

export interface IEvent {
    eventId?: string;
    spread?: {
        sides: ISide[]
        hold: number;
    };
    total?: {
        sides: ISide[],
        hold: number;
    },
    moneyline?: {
        sides: ISide[],
        hold: number;
    }
    simulationResults?: ISimulationResults;
}

export interface ISimulationResults {
    homeMLEdge: number;
    homeMLPct: number;
    homeSpreadEdge: number;
    homeSpreadPct: number;
    overEdge: number;
    overPct: number;
    underEdge: number;
    underPct: number;
    awayMLEdge: number;
    awayMLPct: number;
    awaySpreadEdge: number;
    awaySpreadPct: number;
}