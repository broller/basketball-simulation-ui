import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { Observable, zip } from "rxjs";
import { map, take } from "rxjs/operators";
import { WagerTypesEnum } from "../shared/common/enums/wager-types.enum";
import { Team } from "../shared/common/interfaces/teams.interface";
import { EventsRecordsWriterService } from "../shared/records/events-records-writer.service";
import { IEventResponse } from "./interfaces/event-response.interface";
import { PaceModelService } from "./pace/services/pace-model.service";

@Component({
    selector: 'models-container',
    templateUrl: './models-container.component.html',
    styleUrls: ['./models-container.component.scss']
})
export class ModelsContainerComponent implements OnInit {
    public teams: Team[];
    public modelResults: IEventResponse;
    public matchup = {away: null, home: null};
    public filterKey: string;

    constructor(
        private eventsRecordsWriterService: EventsRecordsWriterService,
        private paceModelService: PaceModelService, 
        private http: HttpClient) {}

    ngOnInit(): void {
        zip(this._getTeams(), this._getModelResults())
            .pipe(take(1)).subscribe( ([teams, modelResults]) => {
                this.teams = teams;
                this.modelResults = modelResults;
            });
    }

    private _getModelResults(): Observable<IEventResponse> {
        return this.paceModelService.getPaceModelResults();
    }

    private _getTeams(): Observable<Team[]> {
        return this.http.get('//localhost:8080/sports/NBA/teams')
            .pipe(
                map((teams: Team[]) => {
                    return teams.sort((a, b) => {
                        var textA = a.teamName.toUpperCase();
                        var textB = b.teamName.toUpperCase();
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    });
                })
            );
    }

    public onTeamSelect($event) {
        if(this.matchup.away && this.matchup.home) {
            this.filterKey = this.matchup.away + '/' + this.matchup.home;
        } else {
            this.filterKey = null;
        }
    }

    public saveWagers() {
        const records = [];
        for(let affiliate in this.modelResults) {
            this.modelResults[affiliate].forEach(hold => {
                for(let market in hold) {
                    if(!!hold[market]?.sides) {
                        hold[market].sides?.forEach( (side, index) => {
                            if(side.pick) {
                                const record = this.eventsRecordsWriterService.createRecord(market as WagerTypesEnum, hold, index);
                                records.push(record);
                            }  
                        });
                    }
                }
            });
        }
        // save here via service
        this.eventsRecordsWriterService.submitRecords(records)
            .pipe(take(1)).subscribe(success => {
                console.error(success);
            });
    }
}