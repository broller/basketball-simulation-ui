import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HoldsComponent } from './holds/holds.component';
import { ModelsContainerComponent } from './models/models-container.component';
import { StatsContainerComponent } from './stats/components/stats-container/stats.container.component';

const routes: Routes = [
  {path: 'holds', component: HoldsComponent},
  {path: 'stats', component: StatsContainerComponent},
  {path: 'models', component: ModelsContainerComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
