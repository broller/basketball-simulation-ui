export enum WagerTypesEnum {
    SPREAD = 'spread',
    TOTAL = 'total',
    MONEYLINE = 'moneyline'
}

export enum TotalsEnum {
    OVER = 'OVER',
    UNDER = 'UNDER'
}