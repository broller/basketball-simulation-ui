export interface Team {
    key: string;
    teamName: string;
}