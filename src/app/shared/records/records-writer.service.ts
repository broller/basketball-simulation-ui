import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { IHolds, ISide } from "src/app/holds/interfaces/holds.interface";
import { WagerTypesEnum } from "../common/enums/wager-types.enum";
import { IRecord } from "./interfaces/record-interace";

@Injectable({ providedIn: 'root'})
export class RecordsWriterService {
    constructor(private httpClient: HttpClient) {}

    public createRecord(wagerType: WagerTypesEnum, hold: IHolds, pickIndex: number): IRecord {
       switch(wagerType) {
            case WagerTypesEnum.SPREAD:
               return this._createSpreadRecord(wagerType, hold, pickIndex);
            case WagerTypesEnum.TOTAL:
                return this._createTotalRecord(wagerType, hold, pickIndex);
            case WagerTypesEnum.MONEYLINE:
                return this._createMoneylineRecord(wagerType, hold, pickIndex);
       }
       return null;
    }

    public submitRecords(records: IRecord[]): Observable<any> {
        const options = {headers: {'Content-Type': 'application/json'}};
        return this.httpClient.post<IRecord[]>('//localhost:8080/records/update', records, options);
    }

    private _createSpreadRecord(wagerType: WagerTypesEnum, hold: IHolds, pickIndex: number): IRecord {
        const teams = this._getFavoriteIndex(hold.moneyline.sides);
        const favorite = hold[wagerType].sides[teams.favorite];
        const underdog = hold[wagerType].sides[teams.underdog];

        const edge = teams.favorite === pickIndex ? hold.simulation.favoriteSpreadEdge : hold.simulation.underdogSpreadEdge
        return { market: wagerType, favorite: favorite.teamName, underdog: underdog.teamName,
            spread: hold[wagerType].sides[pickIndex].points, odds: hold[wagerType].sides[pickIndex].odds, edge}
    }

    private _createTotalRecord(wagerType: WagerTypesEnum, hold: IHolds, pickIndex: number): IRecord {
        const teams = this._getFavoriteIndex(hold.moneyline.sides);
        const favorite = hold[wagerType].sides[teams.favorite];
        const underdog = hold[wagerType].sides[teams.underdog];

        const edge = hold[wagerType].sides[pickIndex].overUnder.toLowerCase() === 'o' ? hold.simulation.overEdge : hold.simulation.underEdge;
        return { market: wagerType, favorite: favorite.teamName, underdog: underdog.teamName,
            total: favorite.points, odds: hold[wagerType].sides[pickIndex].odds, pick: hold[wagerType].sides[pickIndex].overUnder, edge };
    }

    private _createMoneylineRecord(wagerType: WagerTypesEnum, hold: IHolds, pickIndex: number): IRecord {
        const teams = this._getFavoriteIndex(hold.moneyline.sides);
        const favorite = hold[wagerType].sides[teams.favorite];
        const underdog = hold[wagerType].sides[teams.underdog];

        const edge = teams.favorite === pickIndex ? hold.simulation.favoriteMLEdge : hold.simulation.underdogMLEdge;
        return { market: wagerType, favorite: favorite.teamName, underdog: underdog.teamName,
           odds: hold[wagerType].sides[pickIndex].odds, pick: hold[wagerType].sides[pickIndex].teamName, edge };
    }

    private _getFavoriteIndex(sides: ISide[]): {favorite: number, underdog: number} {
        if(sides[0].odds < sides[1].odds) {
            return {favorite: 0, underdog: 1};
        } else {
            return {favorite: 1, underdog: 0};
        }
    }
}