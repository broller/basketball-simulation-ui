import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { IEvent } from "src/app/models/interfaces/event.interface";
import { WagerTypesEnum } from "../common/enums/wager-types.enum";
import { IEventRecord } from "./interfaces/event-record.interface";

@Injectable({ providedIn: 'root'})
export class EventsRecordsWriterService {
    private readonly AWAY = 0;
    private readonly HOME = 1;

    constructor(private httpClient: HttpClient) {}

    public submitRecords(records: IEventRecord[]): Observable<any> {
        const options = {headers: {'Content-Type': 'application/json'}};
        return this.httpClient.post<IEventRecord[]>('//localhost:8080/records/update/models/pace', records, options);
    }

    public createRecord(wagerType: WagerTypesEnum, event: IEvent, pickIndex: number): IEventRecord {
        switch(wagerType) {
             case WagerTypesEnum.SPREAD:
                return this._createSpreadRecord(wagerType, event, pickIndex);
             case WagerTypesEnum.TOTAL:
                 return this._createTotalRecord(wagerType, event, pickIndex);
             case WagerTypesEnum.MONEYLINE:
                 return this._createMoneylineRecord(wagerType, event, pickIndex);
        }
        return null;
    }

    private _createSpreadRecord(wagerType: WagerTypesEnum, hold: IEvent, pickIndex: number): IEventRecord {
        const awayTeam = hold[wagerType].sides[this.AWAY];
        const homeTeam = hold[wagerType].sides[this.HOME];

        const edge = this.HOME === pickIndex ? hold.simulationResults.homeSpreadEdge : hold.simulationResults.awaySpreadEdge;
        return { market: wagerType, home: homeTeam.teamName, away: awayTeam.teamName, pick: hold[wagerType].sides[pickIndex].teamName,
            spread: hold[wagerType].sides[this.HOME].points, odds: hold[wagerType].sides[pickIndex].odds, edge}
    }

    private _createTotalRecord(wagerType: WagerTypesEnum, hold: IEvent, pickIndex: number): IEventRecord {
        const awayTeam = hold[wagerType].sides[this.AWAY];
        const homeTeam = hold[wagerType].sides[this.HOME];

        const edge = hold[wagerType].sides[pickIndex].overUnder.toLowerCase() === 'o' ? hold.simulationResults.overEdge : hold.simulationResults.underEdge;
        return { market: wagerType, home: homeTeam.teamName, away: awayTeam.teamName,
            total: homeTeam.points, odds: hold[wagerType].sides[pickIndex].odds, pick: hold[wagerType].sides[pickIndex].overUnder, edge };
    }

    private _createMoneylineRecord(wagerType: WagerTypesEnum, hold: IEvent, pickIndex: number): IEventRecord {
        const awayTeam = hold[wagerType].sides[this.AWAY];
        const homeTeam = hold[wagerType].sides[this.HOME];

        const edge = this.HOME === pickIndex ? hold.simulationResults.homeMLEdge : hold.simulationResults.awayMLEdge;
        return { market: wagerType, home: homeTeam.teamName, away: awayTeam.teamName,
           odds: hold[wagerType].sides[pickIndex].odds, pick: hold[wagerType].sides[pickIndex].teamName, edge };
    }
}