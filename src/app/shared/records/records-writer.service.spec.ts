import { HttpClient } from "@angular/common/http";
import { TestBed, inject } from "@angular/core/testing"
import { Observable, of } from "rxjs";
import { IHolds } from "src/app/holds/interfaces/holds.interface";
import { WagerTypesEnum } from "../common/enums/wager-types.enum";
import { RecordsWriterService } from "./records-writer.service";

const mockHold: IHolds = {
    "total": {
        "sides": [
            {
                "teamName": "Utah Jazz",
                "points": 222.5,
                "odds": -110,
                "overUnder": "O"
            },
            {
                "teamName": "Los Angeles Clippers",
                "points": 222.5,
                "odds": -110,
                "overUnder": "U",
                "pick": true
            }
        ],
        "hold": 0.045454545454545456
    },
    "moneyline": {
        "sides": [
            {
                "teamName": "Utah Jazz",
                "points": 0,
                "odds": 172,
                "overUnder": null,
                "pick": true
            },
            {
                "teamName": "Los Angeles Clippers",
                "points": 0,
                "odds": -205,
                "overUnder": null
            }
        ],
        "hold": 0.03825643403663344
    },
    "spread": {
        "sides": [
            {
                "teamName": "Utah Jazz",
                "points": 5,
                "odds": -114,
                "overUnder": null,
                "pick": true
            },
            {
                "teamName": "Los Angeles Clippers",
                "points": -5,
                "odds": -106,
                "overUnder": null
            }
        ],
        "hold": 0.0451394905562294
    },
    "simulation": {
        "favoriteMLPct": 0.41658,
        "favoriteMLEdge": -0.38,
        "underdogMLPct": 0.58342,
        "underdogMLEdge": 0.58,
        "overPct": 0.46098,
        "overEdge": -0.12,
        "underPct": 0.53902,
        "underEdge": 0.029,
        "favoriteSpreadPct": 0.30351,
        "favoriteSpreadEdge": -0.41,
        "underdogSpreadPct": 0.69649,
        "underdogSpreadEdge": 0.30
    }
}

class MockHttpClient {
    post<IMonteCarloResults>(url: String, formObject: any): Observable<any> {
        return of(formObject);
    }
}

describe('Records Writer Service', () => {
    let hold: IHolds;
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {provide: HttpClient, useClass: MockHttpClient},
                RecordsWriterService
            ],
        })
        hold = mockHold;
    });

    it('should exist', inject([RecordsWriterService], (service: RecordsWriterService) => {
        expect(service).toBeTruthy();
    }));

    it('should create a correct spread record for an underdog', inject([RecordsWriterService], (service: RecordsWriterService) => {
        const record = service.createRecord(WagerTypesEnum.SPREAD, hold, 0);
        expect(record.market).toEqual(WagerTypesEnum.SPREAD);
        expect(record.favorite).toEqual("Los Angeles Clippers");
        expect(record.underdog).toEqual("Utah Jazz");
        expect(record.spread).toEqual(5);
        expect(record.odds).toEqual(-114);
        expect(record.edge).toEqual(0.30);
    }));

    it('should create a correct spread record for a favorite', inject([RecordsWriterService], (service: RecordsWriterService) => {
        hold.spread.sides[0].pick = false;
        hold.spread.sides[1].pick = true;
        const record = service.createRecord(WagerTypesEnum.SPREAD, hold, 1);
        expect(record.market).toEqual(WagerTypesEnum.SPREAD);
        expect(record.favorite).toEqual("Los Angeles Clippers");
        expect(record.underdog).toEqual("Utah Jazz");
        expect(record.spread).toEqual(-5);
        expect(record.odds).toEqual(-106);
        expect(record.edge).toEqual(-0.41);
    }));

    it('should create a correct total record for an over', inject([RecordsWriterService], (service: RecordsWriterService) => {
        hold.total.sides[0].pick = false;
        hold.total.sides[1].pick = true;
        const record = service.createRecord(WagerTypesEnum.TOTAL, hold, 0);
        expect(record.market).toEqual(WagerTypesEnum.TOTAL);
        expect(record.favorite).toEqual("Los Angeles Clippers");
        expect(record.underdog).toEqual("Utah Jazz");
        expect(record.total).toEqual(222.5);
        expect(record.odds).toEqual(-110);
        expect(record.edge).toEqual(-0.12);
        expect(record.pick).toEqual('O');
    }));

    it('should create a correct total record for an under', inject([RecordsWriterService], (service: RecordsWriterService) => {
        const record = service.createRecord(WagerTypesEnum.TOTAL, hold, 1);
        expect(record.market).toEqual(WagerTypesEnum.TOTAL);
        expect(record.favorite).toEqual("Los Angeles Clippers");
        expect(record.underdog).toEqual("Utah Jazz");
        expect(record.total).toEqual(222.5);
        expect(record.odds).toEqual(-110);
        expect(record.edge).toEqual(0.029);
        expect(record.pick).toEqual('U');
    }));

    it('should create a correct moneyline record for a favorite', inject([RecordsWriterService], (service: RecordsWriterService) => {
        hold.moneyline.sides[0].pick = false;
        hold.moneyline.sides[1].pick = true;
        const record = service.createRecord(WagerTypesEnum.MONEYLINE, hold, 1);
        expect(record.market).toEqual(WagerTypesEnum.MONEYLINE);
        expect(record.favorite).toEqual("Los Angeles Clippers");
        expect(record.underdog).toEqual("Utah Jazz");
        expect(record.pick).toEqual("Los Angeles Clippers");
        expect(record.odds).toEqual(-205);
        expect(record.edge).toEqual(-0.38);
    }));

    it('should create a correct moneyline record for an underdog', inject([RecordsWriterService], (service: RecordsWriterService) => {
        const record = service.createRecord(WagerTypesEnum.MONEYLINE, hold, 0);
        expect(record.market).toEqual(WagerTypesEnum.MONEYLINE);
        expect(record.favorite).toEqual("Los Angeles Clippers");
        expect(record.underdog).toEqual("Utah Jazz");
        expect(record.pick).toEqual("Utah Jazz");
        expect(record.odds).toEqual(172);
        expect(record.edge).toEqual(0.58);
    }));
});