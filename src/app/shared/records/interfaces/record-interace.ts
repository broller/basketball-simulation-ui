import { WagerTypesEnum } from "../../common/enums/wager-types.enum";

export interface IRecord {
    market: WagerTypesEnum;
    favorite: String;
    underdog: String;
    odds: Number;
    edge: Number;
    spread?: Number;
    total?: Number;
    pick?: String;
}