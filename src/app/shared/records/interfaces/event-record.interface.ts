import { WagerTypesEnum } from "../../common/enums/wager-types.enum";

export interface IEventRecord {
    market: WagerTypesEnum;
    home: String;
    away: String;
    odds: Number;
    edge: Number;
    spread?: Number;
    total?: Number;
    pick?: String;
}