# Basketball Simulation UI
Front-end component of the the basketball monte carlo simulation projects. First start, the [Analytics Backend](https://bitbucket.org/broller/analytics-backend/src/master/) project, then run `npm start`. Content is served from `http://localhost:4200`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
